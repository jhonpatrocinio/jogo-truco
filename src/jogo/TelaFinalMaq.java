package jogo;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class TelaFinalMaq implements ActionListener{
	JFrame frame = new JFrame("Ganhador da Partida");
	JPanel pgeral = new JPanel();
	ImageIcon imgganhador = new ImageIcon(getClass().getResource("img/ibm-pc-final.png"));
	JLabel lbfoto = new JLabel(imgganhador);
	JButton menu = new JButton("Voltar para Menu");
	JPanel pbotoes = new JPanel();
	
	public TelaFinalMaq() {
		pgeral.add(lbfoto);
		frame.add(pgeral, BorderLayout.CENTER);
		pbotoes.add(menu);
		frame.add(pbotoes, BorderLayout.PAGE_END);
		
		imgganhador.setImage(imgganhador.getImage().getScaledInstance(600, 600, 600));

		//Escutadores
		menu.addActionListener(this);


		frame.setSize(500, 500);
		frame.setResizable(false);
		frame.pack(); //Ajustando a TELA de Acordo com os Componentes
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);  // Ajusta a Janela no MEIO da TELA
		//frame.setVisible(true); //Deixar Visivel ao USUARIO
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == menu) {
			MenuJogo back = new MenuJogo();
			frame.dispose();
			back.frame.setVisible(true);
		}
	}
}