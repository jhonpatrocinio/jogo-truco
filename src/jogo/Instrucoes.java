package jogo;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Instrucoes implements ActionListener {
	JFrame frame = new JFrame("Instruções");
	JPanel pfoto = new JPanel();
	Icon imginstru = new ImageIcon(getClass().getResource("img/instrucoes.png"));
	JLabel lbfoto = new JLabel(imginstru);
	JButton voltar = new JButton("Voltar para o menu");



	public Instrucoes() {
		frame.add(pfoto, BorderLayout.CENTER);
		pfoto.add(lbfoto);
		frame.add(voltar, BorderLayout.PAGE_END);

		//Escutador de Eventos
		voltar.addActionListener(this);


		//frame.setSize(500, 500);
		frame.setResizable(false);
		frame.pack(); //Ajustando a TELA de Acordo com os Componentes
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);  // Ajusta a Janela no MEIO da TELA
		frame.setVisible(true); //Deixar Visivel ao USUARIO
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() == voltar) {
			MenuJogo menu = new MenuJogo();
			frame.dispose();
			menu.frame.setVisible(true);
		}
	}
}
