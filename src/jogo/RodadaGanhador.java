package jogo;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class RodadaGanhador implements ActionListener {
	JDialog frame = new JDialog();
	JPanel pTexto = new JPanel();
	JPanel pBotao = new JPanel();
	JPanel pNada = new JPanel();
	JLabel lbTexto = new JLabel();
	Icon imgok = new ImageIcon(getClass().getResource("img/icons8-ok-50.png")); 
	JLabel lbfoto = new JLabel(imgok);
	JButton btOk = new JButton("OK");
	public RodadaGanhador(String Nome2) {
		
		frame.add(pNada, BorderLayout.PAGE_START);
		frame.add(lbfoto, BorderLayout.LINE_START);
		pTexto.setLayout(new BorderLayout());
		lbTexto.setText("   O Jogador "+Nome2+" ganhou essa Rodada   ");
		pTexto.add(lbTexto, BorderLayout.EAST);
		pBotao.add(btOk);
		frame.add(pTexto, BorderLayout.LINE_END);
		frame.add(btOk, BorderLayout.PAGE_END);
		
		
		//Escutador 
		btOk.addActionListener(this);
		
		//frame.setSize(300, 130);
		frame.setModal(true);
		frame.setResizable(false);
		frame.pack(); //Ajustando a TELA de Acordo com os Componentes
		frame.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		frame.setLocation(1200, 500);
		//frame.setVisible(true); //Deixar Visivel ao USUARIO
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() == btOk) {
			frame.dispose();
		}
	}

}
