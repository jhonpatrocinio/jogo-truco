package jogo;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MenuJogo implements ActionListener{	

	//Declaraando os Componentes
	//INICIO
	JDialog dialog = new JDialog();
	JFrame frame = new JFrame("TRUUUCO");
	JPanel pinicio = new JPanel();
	Icon imginicio = new ImageIcon(getClass().getResource("img/main.jpg"));
	JLabel lbfoto = new JLabel(imginicio);
	JButton btinicar = new JButton("Iniciar Jogo");
	JButton btInstru = new JButton("Instruçoes do Jogo");
	JButton btSair = new JButton("Sair");

	public MenuJogo() {

		//Painel do Inicio
		frame.add(pinicio, BorderLayout.CENTER);
		BorderLayout border = new BorderLayout();
		pinicio.setLayout(border);
		pinicio.add(lbfoto, BorderLayout.PAGE_START);
		pinicio.add(btinicar, BorderLayout.CENTER);
		pinicio.add(btInstru, BorderLayout.LINE_START);
		pinicio.add(btSair, BorderLayout.LINE_END);
		//btinicar.setBounds(20, 250, 150, 25);
		//btInstru.setBounds(20, 300, 150, 25);

		//Escutador de EVentos
		btinicar.addActionListener(this);
		btInstru.addActionListener(this);
		btSair.addActionListener(this); 

		frame.setSize(700, 500);
		frame.setResizable(false);
		frame.pack(); //Ajustando a TELA de Acordo com os Componentes
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);  // Ajusta a Janela no MEIO da TELA
		frame.setVisible(true); //Deixar Visivel ao USUARIO



	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new MenuJogo();
	}

	@Override
	public void actionPerformed(ActionEvent evento) {
		if(evento.getSource()== btinicar) {
			PegaNome pn = new PegaNome();
			frame.dispose();
			pn.frame.setVisible(true);
		}
		if(evento.getSource() == btSair) {
			frame.dispose();
		}
		if(evento.getSource() == btInstru) {
			Instrucoes instru = new Instrucoes();
			frame.dispose();
			instru.frame.setVisible(true);
		}
	}

}
