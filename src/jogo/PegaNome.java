package jogo;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class PegaNome implements ActionListener {

	JFrame frame = new JFrame("Digite seu Nome ");
	JPanel ptotal = new JPanel();
	JLabel lbnome = new JLabel("Nome: ");
	JTextField tfnome = new JTextField(20);
	JButton btEnviar = new JButton("JOGAR!");
	String NomeJogador;
	public PegaNome(){
		frame.add(ptotal, BorderLayout.LINE_START);
		ptotal.add(lbnome);
		ptotal.add(tfnome);
		frame.add(btEnviar, BorderLayout.PAGE_END);
		Font font = new Font("Arial", 1, 20);
		lbnome.setFont(font);
		tfnome.setFont(font);
		btEnviar.setFont(font);



		btEnviar.addActionListener(this);

		frame.setResizable(false);
		frame.pack(); //Ajustando a TELA de Acordo com os Componentes
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);  // Ajusta a Janela no MEIO da TELA
		frame.setVisible(true); //Deixar Visivel ao USUARIO
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == btEnviar) {
			NomeJogador = tfnome.getText();
			Jogo novojogo = new Jogo(NomeJogador);
			frame.dispose();
			novojogo.frame.setVisible(true);

		}

	}
}
