package jogo;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Jogo implements ActionListener{
	//Declaração dos COmponentes
	JFrame frame = new JFrame();
	JPanel pGeral = new JPanel();
	JPanel pJogador = new JPanel();
	JPanel pMaquina = new JPanel();
	JPanel pPlacar = new JPanel();
	JPanel pPlacar2 = new JPanel();
	JPanel pCartas = new JPanel();
	JPanel pCartasjogadas = new JPanel();
	JPanel pjogador1 = new JPanel();
	JPanel pjogador2 = new JPanel();
	JPanel pjogador3 = new JPanel();
	JLabel lbPlacarMaq = new JLabel();
	JLabel lbPlacarHum = new JLabel();
	JLabel lbRodada = new JLabel("Rodada 1");
	//JLabel lbPlacarGeral = new JLabel("Placar Geral: 0");
	Icon imgcartamaq = new ImageIcon(getClass().getResource("img/back-blue-2.png"));
	JLabel lbcartajogada = new JLabel(imgcartamaq);
	JButton btmaqcarta1 = new JButton(imgcartamaq);
	JButton btmaqcarta2 = new JButton(imgcartamaq);
	JButton btmaqcarta3 = new JButton(imgcartamaq);

	JButton btjogcarta1 = new JButton();
	JButton btjogcarta2 = new JButton();
	JButton btjogcarta3 = new JButton();
	//Cartas dos Jogadores
	Icon imgcarta2 = new ImageIcon(getClass().getResource("img/clubs-2-75.png"));
	Icon imgcarta3 = new ImageIcon(getClass().getResource("img/clubs-3-75.png"));
	Icon imgcarta4 = new ImageIcon(getClass().getResource("img/clubs-4-75.png"));
	Icon imgcarta5 = new ImageIcon(getClass().getResource("img/clubs-5-75.png"));
	Icon imgcarta6 = new ImageIcon(getClass().getResource("img/clubs-6-75.png"));
	Icon imgcarta7 = new ImageIcon(getClass().getResource("img/clubs-7-75.png"));
	Icon imgcartaA = new ImageIcon(getClass().getResource("img/clubs-a-75.png"));
	Icon imgcartaJ = new ImageIcon(getClass().getResource("img/clubs-j-75.png"));
	Icon imgcartaK = new ImageIcon(getClass().getResource("img/clubs-k-75.png"));
	Icon imgcartaQ = new ImageIcon(getClass().getResource("img/clubs-q-75.png"));
	//Iniciando Variaveis do jogo
	String Nome2 = "";
	int pontohum = 0;
	int pontomaq = 0;
	int rodadas = 1;
	int placarhumano = 0;
	int placarmaquina = 0;
	int jogadorhumano[] = {-1, -1, -1};
	int jogadormaquina[] = {-1, -1, -1};
	public Jogo(String NomeJogador) {


		Nome2 = NomeJogador;
		// Cartas equivalentes a posição no vetor
		//Posição 0 Carta 4; Posição 1 Carta 5; Posição 2 Carta 5; Posição 3 Carta 6; Posição 4 Carta 7; Posição 5 Carta Q; Posição 6 Carta J; Posição 7 Carta K; Posição 8 Carta A;
		//Posição 9 Carta 2; Posição 10 Carta 3;
		//	int vetorcartas[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

		/*			System.out.println("Cartas do Jogador Humano\n");
				for (int i = 0; i < jogadorhumano.length; i++) {
					System.out.println(jogadorhumano[i]);
				}
				System.out.println("Cartas do Jogador Maquina\n");
				for (int i = 0; i < jogadormaquina.length; i++) {
					System.out.println(jogadormaquina[i]);
				}*/

		//Placar do JOGO
		//Placar de Cima
		BorderLayout bord = new BorderLayout();
		pPlacar.setLayout(bord);
		pPlacar.add(lbPlacarMaq, BorderLayout.WEST);
		//pPlacar.add(lbPlacarGeral, BorderLayout.EAST);
		frame.add(pPlacar, BorderLayout.PAGE_START);
		lbPlacarMaq.setText("Placar da Maquina: "+placarmaquina);

		//Placar do Jogador - Placar de baixo
		pPlacar2.setLayout(new BorderLayout());
		pPlacar2.add(lbPlacarHum, BorderLayout.WEST);
		pPlacar2.add(lbRodada, BorderLayout.EAST);
		lbPlacarHum.setText("Placar de "+NomeJogador+": "+placarhumano);
		frame.add(pPlacar2, BorderLayout.PAGE_END);

		//Paineis do Botao da Maquina e Botoes
		pMaquina.add(btmaqcarta1); pMaquina.add(btmaqcarta2); pMaquina.add(btmaqcarta3);
		//frame.add(pMaquina, BorderLayout.CENTER);
		
		//Painel das Cartas 
		pCartas.setLayout(new BorderLayout());
		pCartas.add(pMaquina, BorderLayout.NORTH);


		//Adicionando as Cartas aos Botoes
		//Cada carta com um painel
		pjogador1.add(btjogcarta1);
		pjogador2.add(btjogcarta2);
		pjogador3.add(btjogcarta3);
		
		//Painel do Jogador e das Cartas
		pJogador.add(pjogador1);
		pJogador.add(pjogador2);
		pJogador.add(pjogador3);
		
		//Chama a Primeira Rodada do Jogo
		Rodada();
		
		pCartas.add(pJogador, BorderLayout.SOUTH);
		pCartasjogadas.setLayout(new GridLayout(1, 1));
		pCartasjogadas.add(lbcartajogada);

		pCartas.add(pCartasjogadas,	BorderLayout.CENTER);
		frame.add(pCartas, BorderLayout.CENTER);
		//pJogador.setLayout(new GridLayout(1, 3));
		//frame.add(pJogador, BorderLayout.SOUTH);


		//Escutador de Eventos
		btjogcarta1.addActionListener(this);
		btjogcarta2.addActionListener(this);
		btjogcarta3.addActionListener(this);

		frame.setSize(1200, 800);
		frame.setResizable(false);
		//frame.pack(); //Ajustando a TELA de Acordo com os Componentes
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);  // Ajusta a Janela no MEIO da TELA
		frame.setVisible(true); //Deixar Visivel ao USUARIO

	}
	public void Rodada() {
		Random gerador = new Random();

		for (int i = 0; i < jogadorhumano.length; i++) {
			jogadorhumano[i] = gerador.nextInt(10);
		}
		for (int i = 0; i < jogadormaquina.length; i++) {
			jogadormaquina[i] = gerador.nextInt(10);
		}

		if(jogadorhumano[0] == 0 ) {
			btjogcarta1.setIcon(imgcarta4);
		}
		if(jogadorhumano[0] == 1 ) {
			btjogcarta1.setIcon(imgcarta5);
		}
		if(jogadorhumano[0] == 2 ) {
			btjogcarta1.setIcon(imgcarta6);
		}
		if(jogadorhumano[0] == 3 ) {
			btjogcarta1.setIcon(imgcarta7);
		}
		if(jogadorhumano[0] == 4 ) {
			btjogcarta1.setIcon(imgcartaQ);
		}
		if(jogadorhumano[0] == 5 ) {
			btjogcarta1.setIcon(imgcartaJ);
		}
		if(jogadorhumano[0] == 6 ) {
			btjogcarta1.setIcon(imgcartaK);
		}
		if(jogadorhumano[0] == 7 ) {
			btjogcarta1.setIcon(imgcartaA);
		}
		if(jogadorhumano[0] == 8 ) {
			btjogcarta1.setIcon(imgcarta2);
		}
		if(jogadorhumano[0] == 9 ) {
			btjogcarta1.setIcon(imgcarta3);
		}
		//CARTA 2
		if(jogadorhumano[1] == 0 ) {
			btjogcarta2.setIcon(imgcarta4);
		}
		if(jogadorhumano[1] == 1 ) {
			btjogcarta2.setIcon(imgcarta5);
		}
		if(jogadorhumano[1] == 2 ) {
			btjogcarta2.setIcon(imgcarta6);
		}
		if(jogadorhumano[1] == 3 ) {
			btjogcarta2.setIcon(imgcarta7);
		}
		if(jogadorhumano[1] == 4 ) {
			btjogcarta2.setIcon(imgcartaQ);
		}
		if(jogadorhumano[1] == 5 ) {
			btjogcarta2.setIcon(imgcartaJ);
		}
		if(jogadorhumano[1] == 6 ) {
			btjogcarta2.setIcon(imgcartaK);
		}
		if(jogadorhumano[1] == 7 ) {
			btjogcarta2.setIcon(imgcartaA);
		}
		if(jogadorhumano[1] == 8 ) {
			btjogcarta2.setIcon(imgcarta2);
		}
		if(jogadorhumano[1] == 9 ) {
			btjogcarta2.setIcon(imgcarta3);
		}
		//Carta 3

		if(jogadorhumano[2] == 0 ) {
			btjogcarta3.setIcon(imgcarta4);
		}
		if(jogadorhumano[2] == 1 ) {
			btjogcarta3.setIcon(imgcarta5);
		}
		if(jogadorhumano[2] == 2 ) {
			btjogcarta3.setIcon(imgcarta6);
		}
		if(jogadorhumano[2] == 3 ) {
			btjogcarta3.setIcon(imgcarta7);
		}
		if(jogadorhumano[2] == 4 ) {
			btjogcarta3.setIcon(imgcartaQ);
		}
		if(jogadorhumano[2] == 5 ) {
			btjogcarta3.setIcon(imgcartaJ);
		}
		if(jogadorhumano[2] == 6 ) {
			btjogcarta3.setIcon(imgcartaK);
		}
		if(jogadorhumano[2] == 7 ) {
			btjogcarta3.setIcon(imgcartaA);
		}
		if(jogadorhumano[2] == 8 ) {
			btjogcarta3.setIcon(imgcarta2);
		}
		if(jogadorhumano[2] == 9 ) {
			btjogcarta3.setIcon(imgcarta3);
		}

	}
	public void PegarIcon(int vet[], int posicao) {


		if(jogadormaquina[posicao] == 0 ) {
			lbcartajogada.setIcon(imgcarta4);
		}
		if(jogadormaquina[posicao] == 1 ) {
			lbcartajogada.setIcon(imgcarta5);
		}
		if(jogadormaquina[posicao] == 2 ) {
			lbcartajogada.setIcon(imgcarta6);
		}
		if(jogadormaquina[posicao] == 3 ) {
			lbcartajogada.setIcon(imgcarta7);
		}
		if(jogadormaquina[posicao] == 4 ) {
			lbcartajogada.setIcon(imgcartaQ);
		}
		if(jogadormaquina[posicao] == 5 ) {
			lbcartajogada.setIcon(imgcartaJ);
		}
		if(jogadormaquina[posicao] == 6 ) {
			lbcartajogada.setIcon(imgcartaK);
		}
		if(jogadormaquina[posicao] == 7 ) {
			lbcartajogada.setIcon(imgcartaA);
		}
		if(jogadormaquina[posicao] == 8 ) {
			lbcartajogada.setIcon(imgcarta2);
		}
		if(jogadormaquina[posicao] == 9 ) {
			lbcartajogada.setIcon(imgcarta3);
		}

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if(e.getSource() == btjogcarta1) { 
			if(!btjogcarta2.isEnabled() && !btjogcarta3.isEnabled()) { // Se clicar no botao 1 e for o ultimo botao para apertar 
				lbcartajogada.setIcon(btjogcarta1.getIcon());
				btjogcarta1.setEnabled(false);
				PegarIcon(jogadormaquina, 0);
				btmaqcarta1.setEnabled(false);
				if(jogadorhumano[0] > jogadormaquina[0]) {
					pontohum++;
					JanelaGanhador jnganha = new JanelaGanhador();
					jnganha.frame.setVisible(true);
				} else {
					if(jogadorhumano[0] == jogadormaquina[0]) {
						pontohum++;
						pontomaq++;
						//JOptionPane.showMessageDialog(null, " EMPATOUU!", "Empate", JOptionPane.DEFAULT_OPTION);
						JanelaEmpate jnempate = new JanelaEmpate();
						jnempate.frame.setVisible(true);
					} else {
						pontomaq++;
						//JOptionPane.showMessageDialog(null, "VOCÊ PERDEU ESSA!", "Perdedor", JOptionPane.WARNING_MESSAGE);
						JanelaPerdeu jnperdeu = new JanelaPerdeu();
						jnperdeu.frame.setVisible(true);
					}
				}
				if(pontohum > pontomaq) {
					//JOptionPane.showMessageDialog(null, "O Jogador "+Nome2+" ganhou essa Rodada", "Ganhador", JOptionPane.DEFAULT_OPTION);
					RodadaGanhador rdganhador = new RodadaGanhador(Nome2);
					rdganhador.frame.setVisible(true);
					placarhumano++;
					lbPlacarHum.setText("Placar de "+Nome2+": "+placarhumano);
					if(placarhumano == 12 || placarmaquina == 12) {
						if(placarhumano == 12) {
							TelaFinalJog fim = new TelaFinalJog();
							frame.dispose();
							fim.frame.setVisible(true);
						}else {
							TelaFinalMaq fimmaq = new TelaFinalMaq();
							frame.dispose();
							fimmaq.frame.setVisible(true);
						}
					}else {
						Rodada();
						rodadas++;
						lbRodada.setText("Rodada "+rodadas);
						lbcartajogada.setIcon(imgcartamaq);
						btjogcarta1.setEnabled(true); btjogcarta2.setEnabled(true); btjogcarta3.setEnabled(true);
						btmaqcarta1.setEnabled(true); btmaqcarta2.setEnabled(true); btmaqcarta3.setEnabled(true);
						pontohum = 0;
						pontomaq = 0;
					}
				}else {
					if(pontohum == pontomaq) {
						//JOptionPane.showMessageDialog(null, "Acabou empatado +1 ponto para cada jogador", "EMPATE!", JOptionPane.DEFAULT_OPTION);
						RodadaEmpate rdempate = new RodadaEmpate();
						rdempate.frame.setVisible(true);
						placarhumano++;
						placarmaquina++;
						lbPlacarHum.setText("Placar de "+Nome2+": "+placarhumano);
						lbPlacarMaq.setText("Placar da Maquina: "+placarmaquina);
						if(placarhumano == 12 || placarmaquina == 12) {
							if(placarhumano == 12) {
								TelaFinalJog fim = new TelaFinalJog();
								frame.dispose();
								fim.frame.setVisible(true);
							}else {
								TelaFinalMaq fimmaq = new TelaFinalMaq();
								frame.dispose();
								fimmaq.frame.setVisible(true);
							}
						}else {
							Rodada();
							rodadas++;
							lbRodada.setText("Rodada "+rodadas);
							lbcartajogada.setIcon(imgcartamaq);
							btjogcarta1.setEnabled(true); btjogcarta2.setEnabled(true); btjogcarta3.setEnabled(true);
							btmaqcarta1.setEnabled(true); btmaqcarta2.setEnabled(true); btmaqcarta3.setEnabled(true);
							pontohum = 0;
							pontomaq = 0;
						}
					} else {
						//JOptionPane.showMessageDialog(null, "A Maquina ganhou essa Rodada", "PERDEUUU", JOptionPane.WARNING_MESSAGE);
						RodadaPerdedor rdperdedor = new RodadaPerdedor();
						rdperdedor.frame.setVisible(true);
						placarmaquina++;
						lbPlacarMaq.setText("Placar da Maquina: "+placarmaquina);
						if(placarhumano == 12 || placarmaquina == 12) {
							if(placarhumano == 12) {
								TelaFinalJog fim = new TelaFinalJog();
								frame.dispose();
								fim.frame.setVisible(true);
							}else {
								TelaFinalMaq fimmaq = new TelaFinalMaq();
								frame.dispose();
								fimmaq.frame.setVisible(true);
							}
						}else { 
							Rodada();
							rodadas++;
							lbRodada.setText("Rodada "+rodadas);
							lbcartajogada.setIcon(imgcartamaq);
							btjogcarta1.setEnabled(true); btjogcarta2.setEnabled(true); btjogcarta3.setEnabled(true);
							btmaqcarta1.setEnabled(true); btmaqcarta2.setEnabled(true); btmaqcarta3.setEnabled(true);
							pontohum = 0;
							pontomaq = 0;
						}
					}
				}
				//FIM SE FOR O ULTIMO A SER APERTADO
			} else { // SENAO CAI AQUI ONDE AINDA RESTA BOTOES A SER CLICADO
				lbcartajogada.setIcon(btjogcarta1.getIcon());
				btjogcarta1.setEnabled(false);
				PegarIcon(jogadormaquina, 0);
				btmaqcarta1.setEnabled(false);
				if(jogadorhumano[0] > jogadormaquina[0]) {
					pontohum++;
					JanelaGanhador jnganha = new JanelaGanhador();
					jnganha.frame.setVisible(true);
					//JOptionPane.showMessageDialog(null, "VOCÊ GANHOU ESSA!", "Ganhador", JOptionPane.CLOSED_OPTION);
				} else {
					if(jogadorhumano[0] == jogadormaquina[0]) {
						pontohum++;
						pontomaq++;
						//JOptionPane.showMessageDialog(null, " EMPATOUU!", "Empate", JOptionPane.CLOSED_OPTION);
						JanelaEmpate jnempate = new JanelaEmpate();
						jnempate.frame.setVisible(true);
					} else {
						pontomaq++;
						//JOptionPane.showMessageDialog(null, "VOCÊ PERDEU ESSA!", "Perdedor", JOptionPane.CLOSED_OPTION);
						JanelaPerdeu jnperdeu = new JanelaPerdeu();
						jnperdeu.frame.setVisible(true);
					}
				}
			}
		}
		if(e.getSource() == btjogcarta2) {
			if(!btjogcarta1.isEnabled() && !btjogcarta3.isEnabled()) {
				lbcartajogada.setIcon(btjogcarta2.getIcon());
				btjogcarta2.setEnabled(false);
				PegarIcon(jogadormaquina, 1);
				btmaqcarta2.setEnabled(false);
				if(jogadorhumano[1] > jogadormaquina[1]) {
					pontohum++;
					//JOptionPane.showMessageDialog(null, "VOCÊ GANHOU ESSA!", "Ganhador", JOptionPane.DEFAULT_OPTION);
					JanelaGanhador jnganha = new JanelaGanhador();
					jnganha.frame.setVisible(true);
				} else {
					if(jogadorhumano[1] == jogadormaquina[1]) {
						pontohum++;
						pontomaq++;
						//JOptionPane.showMessageDialog(null, " EMPATOUU!", "Empate", JOptionPane.DEFAULT_OPTION);
						JanelaEmpate jnempate = new JanelaEmpate();
						jnempate.frame.setVisible(true);
					} else {
						pontomaq++;
						//JOptionPane.showMessageDialog(null, "VOCÊ PERDEU ESSA!", "Perdedor", JOptionPane.WARNING_MESSAGE);
						JanelaPerdeu jnperdeu = new JanelaPerdeu();
						jnperdeu.frame.setVisible(true);
					}
				}
				if(pontohum > pontomaq) {
					//JOptionPane.showMessageDialog(null, "O Jogador "+Nome2+" ganhou essa Rodada", "Ganhador", JOptionPane.DEFAULT_OPTION);
					RodadaGanhador rdganhador = new RodadaGanhador(Nome2);
					rdganhador.frame.setVisible(true);
					placarhumano++;
					lbPlacarHum.setText("Placar de "+Nome2+": "+placarhumano);
					if(placarhumano == 12 || placarmaquina == 12) {
						if(placarhumano == 12) {
							TelaFinalJog fim = new TelaFinalJog();
							frame.dispose();
							fim.frame.setVisible(true);
						}else {
							TelaFinalMaq fimmaq = new TelaFinalMaq();
							frame.dispose();
							fimmaq.frame.setVisible(true);
						}
					}else {
						Rodada();
						rodadas++;
						lbRodada.setText("Rodada "+rodadas);
						lbcartajogada.setIcon(imgcartamaq);
						btjogcarta1.setEnabled(true); btjogcarta2.setEnabled(true); btjogcarta3.setEnabled(true);
						btmaqcarta1.setEnabled(true); btmaqcarta2.setEnabled(true); btmaqcarta3.setEnabled(true);
						pontohum = 0;
						pontomaq = 0;
					}
				}else {
					if(pontohum == pontomaq) {
						//JOptionPane.showMessageDialog(null, "Acabou empatado +1 ponto para cada jogador", "EMPATE!", JOptionPane.DEFAULT_OPTION);
						RodadaEmpate rdempate = new RodadaEmpate();
						rdempate.frame.setVisible(true);
						placarhumano++;
						placarmaquina++;
						lbPlacarHum.setText("Placar de "+Nome2+": "+placarhumano);
						lbPlacarMaq.setText("Placar da Maquina: "+placarmaquina);
						if(placarhumano == 12 || placarmaquina == 12) {
							if(placarhumano == 12) {
								TelaFinalJog fim = new TelaFinalJog();
								frame.dispose();
								fim.frame.setVisible(true);
							}else {
								TelaFinalMaq fimmaq = new TelaFinalMaq();
								frame.dispose();
								fimmaq.frame.setVisible(true);
							}
						}else {
							Rodada();
							rodadas++;
							lbRodada.setText("Rodada "+rodadas);
							lbcartajogada.setIcon(imgcartamaq);
							btjogcarta1.setEnabled(true); btjogcarta2.setEnabled(true); btjogcarta3.setEnabled(true);
							btmaqcarta1.setEnabled(true); btmaqcarta2.setEnabled(true); btmaqcarta3.setEnabled(true);
							pontohum = 0;
							pontomaq = 0;
						}
					} else {
						//JOptionPane.showMessageDialog(null, "A Maquina ganhou essa Rodada", "PERDEUUU", JOptionPane.WARNING_MESSAGE);
						RodadaPerdedor rdperdedor = new RodadaPerdedor();
						rdperdedor.frame.setVisible(true);
						placarmaquina++;
						lbPlacarMaq.setText("Placar da Maquina: "+placarmaquina);
						if(placarhumano == 12 || placarmaquina == 12) {
							if(placarhumano == 12) {
								TelaFinalJog fim = new TelaFinalJog();
								frame.dispose();
								fim.frame.setVisible(true);
							}else {
								TelaFinalMaq fimmaq = new TelaFinalMaq();
								frame.dispose();
								fimmaq.frame.setVisible(true);
							}
						}else {
							Rodada();
							rodadas++;
							lbRodada.setText("Rodada "+rodadas);
							lbcartajogada.setIcon(imgcartamaq);
							btjogcarta1.setEnabled(true); btjogcarta2.setEnabled(true); btjogcarta3.setEnabled(true);
							btmaqcarta1.setEnabled(true); btmaqcarta2.setEnabled(true); btmaqcarta3.setEnabled(true);
							pontohum = 0;
							pontomaq = 0;
						}
					}
				}
				//FIM SE FOR O ULTIMO A SER APERTADO
			} else { // SENAO CAI AQUI ONDE AINDA RESTA BOTOES A SER CLICADO
				lbcartajogada.setIcon(btjogcarta2.getIcon());
				btjogcarta2.setEnabled(false);
				PegarIcon(jogadormaquina, 1);
				btmaqcarta2.setEnabled(false);
				if(jogadorhumano[1] > jogadormaquina[1]) {
					pontohum++;
					//JOptionPane.showMessageDialog(null, "VOCÊ GANHOU ESSA!", "Ganhador", JOptionPane.CLOSED_OPTION);
					JanelaGanhador jnganha = new JanelaGanhador();
					jnganha.frame.setVisible(true);
				} else {
					if(jogadorhumano[1] == jogadormaquina[1]) {
						pontohum++;
						pontomaq++;
						//JOptionPane.showMessageDialog(null, " EMPATOUU!", "Empate", JOptionPane.CLOSED_OPTION);
						JanelaEmpate jnempate = new JanelaEmpate();
						jnempate.frame.setVisible(true);
					} else {
						pontomaq++;
						//JOptionPane.showMessageDialog(null, "VOCÊ PERDEU ESSA!", "Perdedor", JOptionPane.CLOSED_OPTION);
						JanelaPerdeu jnperdeu = new JanelaPerdeu();
						jnperdeu.frame.setVisible(true);
					}
				}
			}
		}
		if(e.getSource() == btjogcarta3) {
			if(!btjogcarta1.isEnabled() && !btjogcarta2.isEnabled()) {
				lbcartajogada.setIcon(btjogcarta3.getIcon());
				btjogcarta3.setEnabled(false);
				PegarIcon(jogadormaquina, 2);
				btmaqcarta3.setEnabled(false);
				if(jogadorhumano[2] > jogadormaquina[2]) {
					pontohum++;
					//JOptionPane.showMessageDialog(null, "VOCÊ GANHOU ESSA!", "Ganhador", JOptionPane.DEFAULT_OPTION);
					JanelaGanhador jnganha = new JanelaGanhador();
					jnganha.frame.setVisible(true);
				} else {
					if(jogadorhumano[2] == jogadormaquina[2]) {
						pontohum++;
						pontomaq++;
						//JOptionPane.showMessageDialog(null, " EMPATOUU!", "Empate", JOptionPane.DEFAULT_OPTION);
						JanelaEmpate jnempate = new JanelaEmpate();
						jnempate.frame.setVisible(true);
					} else {
						pontomaq++;
						//JOptionPane.showMessageDialog(null, "VOCÊ PERDEU ESSA!", "Perdedor", JOptionPane.WARNING_MESSAGE);
						JanelaPerdeu jnperdeu = new JanelaPerdeu();
						jnperdeu.frame.setVisible(true);
					}
				}
				if(pontohum > pontomaq) {
					//JOptionPane.showMessageDialog(null, "O Jogador "+Nome2+" ganhou essa Rodada", "Ganhador", JOptionPane.DEFAULT_OPTION);
					RodadaGanhador rdganhador = new RodadaGanhador(Nome2);
					rdganhador.frame.setVisible(true);
					placarhumano++;
					lbPlacarHum.setText("Placar de "+Nome2+": "+placarhumano);
					if(placarhumano == 12 || placarmaquina == 12) {
						if(placarhumano == 12) {
							TelaFinalJog fim = new TelaFinalJog();
							frame.dispose();
							fim.frame.setVisible(true);
						}else {
							TelaFinalMaq fimmaq = new TelaFinalMaq();
							frame.dispose();
							fimmaq.frame.setVisible(true);
						}
					}else {
						Rodada();
						rodadas++;
						lbRodada.setText("Rodada "+rodadas);
						lbcartajogada.setIcon(imgcartamaq);
						btjogcarta1.setEnabled(true); btjogcarta2.setEnabled(true); btjogcarta3.setEnabled(true);
						btmaqcarta1.setEnabled(true); btmaqcarta2.setEnabled(true); btmaqcarta3.setEnabled(true);
						pontohum = 0;
						pontomaq = 0;
					}
				}else {
					if(pontohum == pontomaq) {
						//JOptionPane.showMessageDialog(null, "Acabou empatado +1 ponto para cada jogador", "EMPATE!", JOptionPane.DEFAULT_OPTION);
						RodadaEmpate rdempate = new RodadaEmpate();
						rdempate.frame.setVisible(true);
						placarhumano++;
						placarmaquina++;
						lbPlacarHum.setText("Placar de "+Nome2+": "+placarhumano);
						lbPlacarMaq.setText("Placar da Maquina: "+placarmaquina);
						if(placarhumano == 12 || placarmaquina == 12) {
							if(placarhumano == 12) {
								TelaFinalJog fim = new TelaFinalJog();
								frame.dispose();
								fim.frame.setVisible(true);
							}else {
								TelaFinalMaq fimmaq = new TelaFinalMaq();
								frame.dispose();
								fimmaq.frame.setVisible(true);
							}
						}else {
							Rodada();
							rodadas++;
							lbRodada.setText("Rodada "+rodadas);
							lbcartajogada.setIcon(imgcartamaq);
							btjogcarta1.setEnabled(true); btjogcarta2.setEnabled(true); btjogcarta3.setEnabled(true);
							btmaqcarta1.setEnabled(true); btmaqcarta2.setEnabled(true); btmaqcarta3.setEnabled(true);
							pontohum = 0;
							pontomaq = 0;
						}
					} else {
						//JOptionPane.showMessageDialog(null, "A Maquina ganhou essa Rodada", "PERDEUUU", JOptionPane.WARNING_MESSAGE);
						RodadaPerdedor rdperdedor = new RodadaPerdedor();
						rdperdedor.frame.setVisible(true);
						placarmaquina++;
						lbPlacarMaq.setText("Placar da Maquina: "+placarmaquina);
						if(placarhumano == 12 || placarmaquina == 12) {
							if(placarhumano == 12) {
								TelaFinalJog fim = new TelaFinalJog();
								frame.dispose();
								fim.frame.setVisible(true);
							}else {
								TelaFinalMaq fimmaq = new TelaFinalMaq();
								frame.dispose();
								fimmaq.frame.setVisible(true);
							}
						}else {
							Rodada();
							rodadas++;
							lbRodada.setText("Rodada "+rodadas);
							lbcartajogada.setIcon(imgcartamaq);
							btjogcarta1.setEnabled(true); btjogcarta2.setEnabled(true); btjogcarta3.setEnabled(true);
							btmaqcarta1.setEnabled(true); btmaqcarta2.setEnabled(true); btmaqcarta3.setEnabled(true);
							pontohum = 0;
							pontomaq = 0;
						}
					}
				}
				//FIM SE FOR O ULTIMO A SER APERTADO
			} else { // SENAO CAI AQUI ONDE AINDA RESTA BOTOES A SER CLICADO
				lbcartajogada.setIcon(btjogcarta3.getIcon());
				btjogcarta3.setEnabled(false);
				PegarIcon(jogadormaquina, 2);
				btmaqcarta3.setEnabled(false);
				if(jogadorhumano[2] > jogadormaquina[2]) {
					pontohum++;
					//JOptionPane.showMessageDialog(null, "VOCÊ GANHOU ESSA!", "Ganhador", JOptionPane.CLOSED_OPTION);
					JanelaGanhador jnganha = new JanelaGanhador();
					jnganha.frame.setVisible(true);
				} else {
					if(jogadorhumano[2] == jogadormaquina[2]) {
						pontohum++;
						pontomaq++;
						//JOptionPane.showMessageDialog(null, " EMPATOUU!", "Empate", JOptionPane.CLOSED_OPTION);
						JanelaEmpate jnempate = new JanelaEmpate();
						jnempate.frame.setVisible(true);
					} else {
						pontomaq++;
						//JOptionPane.showMessageDialog(null, "VOCÊ PERDEU ESSA!", "Perdedor", JOptionPane.CLOSED_OPTION);
						JanelaPerdeu jnperdeu = new JanelaPerdeu();
						jnperdeu.frame.setVisible(true);
					}
				}
			}
		}
	}

	
	
}
